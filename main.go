package main

import (
	"net/http"
	"github.com/go-redis/redis"
	"github.com/magiconair/properties"
	"encoding/json"
	"bytes"
	"io/ioutil"
)

func initParam()map[string]string{
	return map[string]string{
		"appID": "fc3c36c6-5efb-4d37-b5e8-0bdf87c68d3f",
		"url": "https://onesignal.com/api/v1/notifications",
	}
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/api/digiroin/notification/user", send)
	mux.HandleFunc("/api/digiroin/notification/all", sendAll)
	http.ListenAndServe(":7032", mux)
}

type Notification struct {
	ReceiverId string
	Message string
	Data json.RawMessage
	Title string
	Icon string
}

type NotificationAll struct {
	Message string
	Data json.RawMessage
	Title string
	Icon string
}

func initRedis() *redis.Client{
	p := properties.MustLoadFile("/etc/service.conf", properties.UTF8)
	address := p.GetString("redis.name","localhost")+":"+p.GetString("redis.port","6379")
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: p.GetString("redis.password",""),
		DB:       p.GetInt("redis.db",0),
	})
	return client
}

func send(w http.ResponseWriter, r *http.Request) {
	reqNotification := Notification{}
	err := json.NewDecoder(r.Body).Decode(&reqNotification)
	if err != nil{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
	}else{
		if reqNotification.Message == "" || reqNotification.ReceiverId == "" {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(`{"error" : "bad request"}`))
		}else{
			param := initParam()
			client := &http.Client{}
			var jsonprep string = `{"app_id": "`+param["appID"]+`","include_player_ids": ["`+reqNotification.ReceiverId+`"],"contents": {"en": "`+reqNotification.Message+`"}, "data":`+string(reqNotification.Data)+`,"headings":{"en":"`+reqNotification.Title+`"},"large_icon":"`+reqNotification.Icon+`"}`
			var jsonStr = []byte(jsonprep)
			req, err := http.NewRequest("POST", param["url"], bytes.NewBuffer(jsonStr))
			req.Header.Set("Content-Type", "application/json")
			req.Header.Set("Authorization", "Basic ZjFiMGM1ZjctMTBkNS00MzNkLWFmZjktNDIwOWMwYTJlMjgy")
			resp, err := client.Do(req)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(`{"error" : "`+err.Error()+`"}`))
			} else {
				body, _ := ioutil.ReadAll(resp.Body)
				w.WriteHeader(http.StatusOK)
				w.Write([]byte(body))
			}
		}
	}
}

func sendAll(w http.ResponseWriter, r *http.Request) {
	reqNotification := NotificationAll{}
	err := json.NewDecoder(r.Body).Decode(&reqNotification)
	if err != nil{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
	}else{
		if reqNotification.Message == ""{
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(`{"error" : "bad request"}`))
		}else{
			param := initParam()
			client := &http.Client{}
			var jsonprep string = `{"app_id": "`+param["appID"]+`","included_segments": ["All"],"contents": {"en": "`+reqNotification.Message+`"}, "data":`+string(reqNotification.Data)+`,"headings":{"en":"`+reqNotification.Title+`"},"large_icon":"`+reqNotification.Icon+`"}`
			var jsonStr = []byte(jsonprep)
			req, err := http.NewRequest("POST", param["url"], bytes.NewBuffer(jsonStr))
			req.Header.Set("Content-Type", "application/json")
			req.Header.Set("Authorization", "Basic ZjFiMGM1ZjctMTBkNS00MzNkLWFmZjktNDIwOWMwYTJlMjgy")
			resp, err := client.Do(req)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(`{"error" : "`+err.Error()+`"}`))
			} else {
				body, _ := ioutil.ReadAll(resp.Body)
				w.WriteHeader(http.StatusOK)
				w.Write([]byte(body))
			}
		}
	}
}

